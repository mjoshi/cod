#Author - Manas
#In the following function I will attempt to do the following:
#from the gene_info dataframe get all possible dnabd for a gene, get the sequences of all dnabd for a gene and pass those sequences to the 
#get_synonymous_length function and get the syn and nonsyn length

#first I import gene_info here which contains the required information
gene_info

#######################################################################################################################################
#delete me later
#trial query
server = "http://rest.ensembl.org"
ext = "/sequence/region/arabidopsis_thaliana/5:26187147..26187229/"
r= GET(paste(server,ext,sep = ""), accept("application/JSON"))
response = fromJSON(toJSON(content(r)), flatten = TRUE)

#in the query : X:some_start_position..some_stop_position - where X is the start position of the chromosome while other two are 
#self-explanatory
######################################################################################################################################
gene_info_seq=data.frame()

#making DNABD wise request : every DNABD's individual sequence is stored
#also supplying the sequence to Stefan's code to get the synonymoys length
for (item in 1:nrow(gene_info)){
  row = gene_info[item,]
  
  server = "http://rest.ensembl.org"
  ext = "/sequence/region/arabidopsis_thaliana/"
  r = GET(paste(server,ext,row$seq_region_name,":",row$start_list,"..",row$stop_list,sep=""),accept("application/JSON"))
  response=fromJSON(toJSON(content(r)), flatten = TRUE)
  
  seq = response$seq
  
  syn_length = get_number_of_synonymous_sites(as.character(seq))
  
  df = data.frame(ensembl_id = row$ensembl_id, longest_transcript = row$longest_transcript, TF_name = row$TF_name, TF_family = row$TF_family, 
                  seq_region_name = row$seq_region_name, DNABD_start_position = row$start_list, DNABD_stop_position = row$stop_list, 
                  sequence = seq, syn_length = syn_length)
  
  gene_info_seq = rbind(df, gene_info_seq)
}

#ungapped length here is simply every DNABD specific stop position - start position
gene_info_seq$ungapped_length = gene_info_seq$DNABD_stop_position - gene_info_seq$DNABD_start_position

#nonsyn_length = ungapped_length - synonymous_length
gene_info_seq$non_syn_length = gene_info_seq$ungapped_length - gene_info_seq$syn_length

#subsetting the dataframe now
col_names = c("ensembl_id", "longest_transcript", "TF_name", "TF_family","DNABD_start_position", "DNABD_stop_position", 
              "syn_length", "ungapped_length", "non_syn_length")
gene_info_seq_subset = gene_info_seq[,col_names]
gene_info_seq_subset

#for div_syn_nonsyn_info - colnames = end , most_severe_consequence (synonymous_variant, missense_variant)
#for poly_syn_nonsyn_info - colnames = end, most_severe_consequence (syn/nonsyn)

#for polymorphism : checking how many variants fall with in each binding domain and how many are syn and how many are nonsyn
gene_info_poly = data.frame()

for (item in 1:nrow(gene_info_seq_subset)){
  row = gene_info_seq_subset[item,]
  syn_count = 0
  nonsyn_count = 0
  for (item_1 in 1:nrow(poly_syn_nonsyn_info)){
    row_1 = poly_syn_nonsyn_info[item_1,]
    if (row_1$end %in% seq(row$DNABD_start_position, row$DNABD_stop_position)){
      #print(row_1$end)
      if (row_1$most_severe_consequence =="syn"){
        syn_count = syn_count+1
      }
      else if (row_1$most_severe_consequence == "nonsyn"){
        nonsyn_count = nonsyn_count+1
      }
    }    
  }
  df = data.frame(ensembl_id = row$ensembl_id, longest_transcript = row$longest_transcript, TF_name = row$TF_name, TF_family = row$TF_family,
                  DNABD_start_position = row$DNABD_start_position, DNABD_stop_position = row$DNABD_stop_position, syn_length = row$syn_length,
                  ungapped_length = row$ungapped_length, non_syn_length = row$non_syn_length, poly_syn_count = syn_count, poly_nonsyn_count = nonsyn_count)
  gene_info_poly = rbind(df, gene_info_poly)
}



#same for diversion : checking how many divergence variants fall within each binding domain and how many are syn and nonsyn
gene_info_poly_dive = data.frame()

for (item in 1:nrow(gene_info_poly)){
  row = gene_info_poly[item,]
  syn_count = 0
  nonsyn_count = 0
  for (item_1 in 1:nrow(div_syn_nonsyn_info)){
    row_1 = div_syn_nonsyn_info[item_1,]
    if (as.integer(as.character(row_1$end)) %in% seq(row$DNABD_start_position, row$DNABD_stop_position)){
      #print(row_1$end)
      if (as.character(row_1$most_severe_consequence) =="synonymous_variant"){
        syn_count = syn_count+1
      }
      else if (as.character(row_1$most_severe_consequence) == "missense_variant"){
        nonsyn_count = nonsyn_count+1
      }
    }    
  }
  df = data.frame(ensembl_id = row$ensembl_id, longest_transcript = row$longest_transcript, TF_name = row$TF_name, TF_family = row$TF_family,
                  DNABD_start_position = row$DNABD_start_position, DNABD_stop_position = row$DNABD_stop_position, syn_length = row$syn_length,
                  ungapped_length = row$ungapped_length, non_syn_length = row$non_syn_length, poly_syn_count = row$poly_syn_count,
                  poly_nonsyn_count = row$poly_nonsyn_count, div_syn_count = syn_count, div_nonsyn_count = nonsyn_count)
  gene_info_poly_dive = rbind(df, gene_info_poly_dive)
}

#merge all dnabds for all genes together (add everything)
#subsetting
col_names= c("DNABD_start_position", "DNABD_stop_position")
gene_info_poly_dive_subset = gene_info_poly_dive[,!(names(gene_info_poly_dive) %in% col_names)]

gene_meta_info = data.frame(ensembl_id = gene_info_poly_dive_subset$ensembl_id, longest_transcript = gene_info_poly_dive_subset$longest_transcript,
                            TF_name = gene_info_poly_dive_subset$TF_name, TF_family = gene_info_poly_dive_subset$TF_family)

gene_meta_info = unique(gene_meta_info)
rownames(gene_meta_info) = NULL

gene_stats = data.frame(ensembl_id = gene_info_poly_dive_subset$ensembl_id, syn_length = gene_info_poly_dive_subset$syn_length, ungapped_length = gene_info_poly_dive_subset$ungapped_length,
                        nonsyn_length = gene_info_poly_dive_subset$non_syn_length, poly_syn_count = gene_info_poly_dive_subset$poly_syn_count, poly_nonsyn_count = gene_info_poly_dive_subset$poly_nonsyn_count,
                        div_syn_count = gene_info_poly_dive_subset$div_syn_count, div_nonsyn_count = gene_info_poly_dive_subset$div_nonsyn_count)

gene_stats_pergene = aggregate(.~ensembl_id, data=gene_stats, FUN=sum)

#merging data back
gene_info = merge(gene_meta_info, gene_stats_pergene, by.x = "ensembl_id", by.y="ensembl_id")

#merging info from call to get_polymorphism_stats.R in code dnabd_statistics.R - poly_dnabd_syn/nonsyn_stats
poly_dnabd_syn = poly_dnabd_syn_stats[,-4]
poly_dnabd_nonsyn = poly_dnabd_nonsyn_stats[,-4]

poly_dnabd_syn_nonsyn_merge = merge(poly_dnabd_syn, poly_dnabd_nonsyn, by = "id")
poly_dnabd_syn_nonsyn_merge

#merging this with gene_info
gene_info_new = merge(gene_info, poly_dnabd_syn_nonsyn_merge, by.x="longest_transcript", by.y="id")

#adding div_syn, div_nonsyn and knks
gene_info_new$div_syn = gene_info_new$div_syn_count/gene_info_new$syn_length
gene_info_new$div_nonsyn = gene_info_new$div_nonsyn_count/gene_info_new$nonsyn_length
gene_info_new$knks = gene_info_new$div_nonsyn/gene_info_new$div_syn
