is_unambiguous_ortholog=function(gene_alignment)
{
  #reformat nested list to dataframe
  alignment_df=ldply(gene_alignment, data.frame)
  # extract ranges and declare Range object 
  query=IRanges(alignment_df$alignments.start, alignment_df$alignments.end)
  #count number of overlaps for each interval
  num_of_overlap_vec=countOverlaps(query,query)
  #test if we find more than one overlapp per interval (note: each interval should overlap with himself only)
  is_unambiguous_ortholog=0 # per default it is not
  if(sum(num_of_overlap_vec)==length(query))
  {
    is_unambiguous_ortholog=1
  }
  
  verbose=1
  if(verbose==1 && is_unambiguous_ortholog==0)
  {
    analysisID="TEMP"
    log_file_name=paste(analysisID,"_ambiguous_orthology",sep="")
    #if(file.exists(log_file_name)) file.remove(log_file_name)
    write.table(alignment_df[,-c(5,12)], file=log_file_name, row.names = F, quote = F, append = T, col.names = F)
  }
  
  return(is_unambiguous_ortholog)
}