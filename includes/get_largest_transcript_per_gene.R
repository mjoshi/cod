#this function will only work if the length of the geneID_vector is < 1000. (because of POST constrinats)
# i should make this more robust by splitting this vector is chuncks of 1000 if more than 1000 genes are provided. 
# i have already done this for another POST function. 

get_largest_transcript_per_gene=function(geneID_vector)
{
   
   #define server url
   geneID_vector = geneID_list$geneID
   server <- "https://rest.ensembl.org"
   ext <- "/lookup/id?expand=1;species=arabidopsis_thaliana"
  
   #declare and define body for POSTing
   my_body=toJSON(list(ids=geneID_vector))
  
   #posting to server
   r<-POST(paste(server,ext,sep = ""), content_type("application/json"), accept("application/json"), body = my_body, config = httr_config)
  
   #checking for error status
   stop_for_status(r)
  
   #get content
   response=fromJSON(toJSON(content(r)))
  

  
   #for each gene use transcript with the largest number of exons
   find_largest_transcripts=function(exon_list){
    largest_exon=which.max(lapply(exon_list, function(x) length(x[,1])));
   }
  
   #for each gene find transcript with the largest length
   find_largest_transcripts=function(transcript_df){
     largest_transcript_id=which.max(unlist(transcript_df$end) - unlist(transcript_df$start) + 1);
   }

   #for each gene find number of transcripts
   find_number_of_transcripts=function(transcript_df){
     num_of_transcripts=dim(transcript_df)[1];
   }
  
   #get longest transcript per gene (report Parent and transcript id into a dataframe).
   largest.transcripts=do.call(rbind,lapply(response, function(x) x$Transcript[find_largest_transcripts(x$Transcript),c("Parent","id")]))
   names(largest.transcripts)=c("Parent","ids")
  
   return(largest.transcripts)
}

## here is the code that I tried to counter POST error, later I realise that I am missing s in https in the above code. Reinstating it and commenting my code which goes
#through every id individually, which is below - MJ-16102020
#geneID_vector = geneID_list$geneID
  # #rewriting this function -- apparently ensembl is not supporting a POST here but only GET- meaning
  # #only one id request at a time, tailoring the code accordingly - MJ14102020
  # 
  # #declaring the empty dataframe in which the output will go
  # largest_transcripts = data.frame()
  # 
  # #keeping here the functions which will determine the longest transcript written originally by SL
  # #for each gene use transcript with the largest number of exons
  # find_largest_transcripts=function(exon_list){
  #   largest_exon=which.max(lapply(exon_list, function(x) length(x[,1])));
  # }
  # 
  # #for each gene find transcript with the largest length
  # find_largest_transcripts=function(transcript_df){
  #   largest_transcript_id=which.max(unlist(transcript_df$end) - unlist(transcript_df$start) + 1);
  # }
  # 
  # #for each gene find number of transcripts
  # find_number_of_transcripts=function(transcript_df){
  #  num_of_transcripts=dim(transcript_df)[1];
  # }
  # 
  # #going over every id in a loop
  # for (item in geneID_vector){
  #   
  #   #print(item)
  #   
  #   #requesting the server here
  #   server = "http://rest.ensembl.org/lookup/"
  #   ext = "?expand=1;species=arabidopsis_thaliana"
  #   requestURL = paste(server, item, ext, sep = "")
  #   print(requestURL)
  #   r = GET(requestURL, accept("application/json"), config = httr_config)
  #   
  #   #checking error status
  #   stop_for_status(r)
  #   
  #   #get the content
  #   response = fromJSON(toJSON(content(r)))
  #   
  #   #getting the longest transcript per gene here
  #   largest.transcripts= response$Transcript[find_largest_transcripts(response$Transcript),c("Parent","id")]
  #   names(largest.transcripts)=c("Parent","ids")
  #   
  #   #binding with the original dataframe here
  #   largest_transcripts = rbind(largest_transcripts,largest.transcripts)
  #   
  # }
  # 
  # #returning here now
  # return(largest_transcripts)
#} 