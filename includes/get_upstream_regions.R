get_upstream_region=function(geneID, window_size){
  
  server <- "http://rest.ensemblgenomes.org"
  ext <- paste("/lookup/id/",geneID,"?expand=0", sep="")
  r <- GET(paste(server, ext, sep = ""), content_type("application/json"), config = httr_config)
  stop_for_status(r)
  
  gene_info=fromJSON(toJSON(content(r)))
  
  chr=gene_info$seq_region_name
  #print(gene_info)
  
  if(gene_info$strand==1)
  {
    start_upstream_region=gene_info$start-window_size
    stop_upstream_region=gene_info$start
    
  } else if(gene_info$strand==-1)
  {
    start_upstream_region=gene_info$end
    stop_upstream_region=gene_info$end+window_size
  }
  
  write.table(paste(chr,":",start_upstream_region,"-",stop_upstream_region, sep=""), file="regions_coordinates.txt", quote = F, row.names = F, col.names = F, append = T)
  return(toString(c(chr, start_upstream_region, stop_upstream_region))) 

}