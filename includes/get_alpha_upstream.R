#function to calculate the proportion of adaptive substitution (alpha).

get_alpha_upstream=function(poly_div=poly_div_upstream_gene){
  
  #calculating alpha total
  #get relative neutral divergence
  d_0=sum(poly_div$div_syn)/sum(poly_div$syn_length.x)
  #get relative fucntional divergence
  d=sum(poly_div$div_abs)/sum(poly_div$ungapped_length)
  #get relative number of neutral polymorphims
  p_0=sum(poly_div$P_syn)/sum(poly_div$syn_length.y)
  #get relative number of selected polymorphims
  p=sum(poly_div$P_upstream)/sum(poly_div$window_size)
  
  #calculate alpha
  alpha_total=1-(d_0/d)*(p/p_0)
  
  #calculating alpha per gene
  #get relative neutral divergence
  d_0=poly_div$div_syn/poly_div$syn_length.x
  #get relative fucntional divergence
  d=poly_div$div_abs/poly_div$ungapped_length
  #get relative number of neutral polymorphims
  p_0=poly_div$P_syn/poly_div$syn_length.y
  #get relative number of selected polymorphims
  p=poly_div$P_upstream/poly_div$window_size
  
  #calculate alpha
  alpha_dist=1-(d_0/d)*(p/p_0)
  
  return(list(alpha_total=alpha_total,alpha_dist=alpha_dist))
  
}