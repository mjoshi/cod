summarize_frequencies_dnabd=function(x=NULL, min_freq=NULL, variant_type=NULL, geneID=NULL, location=NULL, poly_gene=NULL){

  #calculating total heterozygosity
  pi=sum(2*x$freq_der*(1-x$freq_der))
  
  #filter out frequencies lower than user-specified value (min_freq)
  derived_frequencies_filtered=x$freq_der[x$freq_der>=min_freq]
  
  #calculating S (or P in the MK framework) : the number of variant sites
  P=length(derived_frequencies_filtered)

  
  #if UPSTREAM DATA
  if(variant_type=="upstream"){
    

    #heterozygosity per base pair
    pi_upstream=pi/window_size #CAUTION: this is a global variable
    
    #formatting upstream-specific results
    results=data.frame(id=geneID, P_upstream=P, pi_upstream=pi_upstream, window_size=window_size)
    
    #if SYNONYMOUS DATA  
    
    } else if(variant_type=="syn" & location =="dnabd"){  
    
      #get size (denominator) for this region
      P_syn_size_dnabd=poly_gene$poly_syn_length_dnabd[poly_gene$id==geneID]

      #heterozygosity per base pair
      pi_syn_dnabd=pi/P_syn_size_dnabd
      #formatting syn-specific results
      results=data.frame(id=geneID, P_syn_dnabd=P, pi_syn_dnabd=pi_syn_dnabd)
    
    } else if(variant_type=="syn" & location =="nondnabd"){  
      
      #get size (denominator) for this region
      P_syn_size_nondnabd=poly_gene$poly_syn_length_nondnabd[poly_gene$id==geneID]
      
      #heterozygosity per base pair
      pi_syn_nondnabd=pi/P_syn_size_nondnabd
      
      #formatting syn-specific results
      results=data.frame(id=geneID, P_syn_nondnabd=P, pi_syn_nondnabd=pi_syn_nondnabd)
      
    #if NON-SYNONYMOUS DATA
    } else if(variant_type=="nonsyn" & location =="dnabd"){
    
      #get size (denominator) for this region
      P_nonsyn_size_dnabd = poly_gene$poly_nonsyn_length_dnabd[poly_gene$id==geneID]

      #heterozygosity per base pair
      pi_nonsyn_dnabd=pi/P_nonsyn_size_dnabd
      
      #formatting syn-specific results
      results=data.frame(id=geneID, P_nonsyn_dnabd=P, pi_nonsyn_dnabd=pi_nonsyn_dnabd)
  
    } else if(variant_type=="nonsyn" & location=="nondnabd"){
  
      #get size (denominator) for this region
      P_nonsyn_size_nondnabd = poly_gene$poly_nonsyn_length_nondnabd[poly_gene$id==geneID]

      #heterozygosity per base pair
      pi_nonsyn_nondnabd=pi/P_nonsyn_size_nondnabd
  
      #formatting syn-specific results
      results=data.frame(id=geneID, P_nonsyn_nondnabd=P, pi_nonsyn_nondnabd=pi_nonsyn_nondnabd)
      
    }

  return(results)

}


get_polymorphism_stats_per_gene_dnabd=function(allele_frequencies=NULL, min_freq, variant_type=NULL, window_size=window_size){
  
  #extracting SNPs of the specified type (upstream ,syn, or nonsyn)
  allele_frequencies=subset(allele_frequencies, allele_frequencies$type==variant_type)
  by(data = allele_frequencies,INDICES = allele_frequencies$geneID,FUN = function(x) summarize_frequencies(x, min_freq = min_freq, variant_type, window_size))
  
}

get_polymorphism_stats_per_gene2_dnabd=function(geneID=NULL, allele_frequencies=NULL, min_freq, variant_type=NULL, location=NULL, poly_gene = NULL){
  
  #extracting SNPs of the specified type (upstream ,syn, or nonsyn)
  allele_frequencies=allele_frequencies[allele_frequencies$type==variant_type & allele_frequencies$geneID==as.character(geneID),]
  #by(data = allele_frequencies,INDICES = allele_frequencies$geneID,FUN = function(x) summarize_frequencies(x, min_freq = min_freq, variant_type, window_size))
  summarize_frequencies_dnabd(allele_frequencies, min_freq = min_freq, variant_type, geneID, location, poly_gene)

}