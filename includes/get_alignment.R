

get_alignment=function(local_region, analysisID=analysisID)

{
  
  local_region=as.matrix(local_region)
  
  #define url of REST server
  server <- "http://rest.ensembl.org"
  formatted_url_line=paste("/alignment/region/arabidopsis_thaliana/",gsub(" ","",format_gene_coordinate(local_region[1:3])),"?method=LASTZ_NET&compact=1;species_set=arabidopsis_thaliana;species_set=arabidopsis_lyrata;compara=plants", sep="")
  
  print(formatted_url_line)

  #retrieving data from server
  r <- GET(paste(server, formatted_url_line, sep = ""), content_type("application/json"))
  
  print(r)

  
  alignments=NULL
  
  if(r$status_code==200)
  {
    

    # use this if you get a simple nested list back, otherwise inspect its structure
    #region=data.frame(t(sapply(content(r),c)))
    
    #get the alignments for this region 
    #alignments=region$alignments
    #trying this instead (previous solution stopped working after Ensembl Release 96)
    alignments=lapply(content(r), list.remove, "tree")
    
    
    #add back the id of the gene adjacent to the current upstream region (keep link between coordinates, id)
    alignments=lapply(alignments, function(x) list.append(x, id=local_region[4], orf_strand=local_region[5]))
    

    
    #return(alignments)

  }
  #error handling
  else if(r$status_code!=200)
  {
    log_file_name=paste(analysisID,"_get_alignment_log_file.txt",sep="")
    cat(paste("Problem in get_alignment line 33","html status code:", r$status_code,";\t", formatted_url_line, "\n"),file = log_file_name, append = T)
  }
  
  return(alignments)
    
}

  
