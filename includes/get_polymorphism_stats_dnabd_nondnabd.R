#Author - Manas
#this function is originally called from main.R
#here I take in dataframe which contains start stop positions of cds (either dnabd or nondnabd) and corresponding transcript and geneID
#I count the syn and nonsyn (polymorphic) counts of respective cds classes and their respective length per gene

get_polymorphism_stats_dnabd_nondnabd = function(gene_info_dnabd,gene_info_nondnabd){
#gene_info_dnabd
#gene_info_nondnabd

  colnames(gene_info_dnabd)[2] = "id"
  colnames(gene_info_nondnabd)[2] = "id"
  
  ##preparing files before work##
  #data for polymorphism is stored in the file frequencies_cds, reloading it here
  load(paste(backup_directory,"/","frequencies_cds", sep=""))

  #since I lose some data (in situation where some genes dont have binding domains), hence subsetting the dataframe to contain
  #only those ids for which I have the information
  #subset for polymorphism also
  poly_syn_nonsyn_info_subset = frequencies_cds[(frequencies_cds$geneID %in% gene_info_dnabd$id),] 


  #subsetting the poly_syn_nonsyn_info here, I do not need other metainfo
  poly_syn_nonsyn_info_subset = poly_syn_nonsyn_info_subset[c("start", "freq_ref", "freq_alt", "maf", "freq_anc", "freq_der", "type", "geneID")]
  colnames(poly_syn_nonsyn_info_subset) = c("end", "freq_ref", "freq_alt", "maf", "freq_anc", "freq_der", "most_severe_consequence", "geneID")

  #changing some data type of columns here for clarity
  poly_syn_nonsyn_info_subset = transform(poly_syn_nonsyn_info_subset, geneID = as.factor(geneID))
  poly_syn_nonsyn_info_subset = transform(poly_syn_nonsyn_info_subset, end = as.integer(as.character(end)))

  ##preparations done##

  poly_syn_nonsyn_info_subset$DNABD = ifelse(sapply(seq_along(poly_syn_nonsyn_info_subset$end), function(i) {
    inds = gene_info_dnabd$start <= poly_syn_nonsyn_info_subset$end[i] & gene_info_dnabd$end >= poly_syn_nonsyn_info_subset$end[i]
    any(inds) & (poly_syn_nonsyn_info_subset$geneID[i] == gene_info_dnabd$id[which.max(inds)])
  }), "YES", "NO")

  #First for counts
  #subsetting info from the poly_syn_nonsyn_info, since it contains too many columns that I do not need
  list_column =c ("geneID", "end", "most_severe_consequence", "DNABD")
  poly_info = poly_syn_nonsyn_info_subset[list_column]

  #counting syn and nonsyn mutations occuring with this dnabd and outside dnabd
  poly_syn_count_nondnabd = c()
  poly_nonsyn_count_nondnabd =c()
  poly_syn_count_dnabd = c()
  poly_nonsyn_count_dnabd = c()
  id=c()

  for (item in (unique(poly_info$geneID))){
    count_syn = poly_info$DNABD[poly_info$geneID == item & poly_info$most_severe_consequence=="syn"]
    poly_syn_count_dnabd = append((length(which(count_syn == "YES"))), poly_syn_count_dnabd)
    poly_syn_count_nondnabd = append((length(which(count_syn == "NO"))), poly_syn_count_nondnabd)
  
    count_nonsyn = poly_info$DNABD[poly_info$geneID == item & poly_info$most_severe_consequence=="nonsyn"]
    poly_nonsyn_count_dnabd = append((length(which(count_nonsyn == "YES"))), poly_nonsyn_count_dnabd)
    poly_nonsyn_count_nondnabd = append((length(which(count_nonsyn == "NO"))), poly_nonsyn_count_nondnabd)
  
    id = append(item,id)
  
  }

  #putting everything into a dataframe
  poly_syn_nonsyn_count = as.data.frame(cbind(id, poly_syn_count_dnabd, poly_nonsyn_count_dnabd, poly_syn_count_nondnabd, poly_nonsyn_count_nondnabd))
  
  #print(poly_syn_nonsyn_count)
  #Second for lengths
  #First for dnabd
  colnames(gene_info_dnabd)[2] = "id"

  poly_length_dnabd=data.frame()

  for (item in 1:nrow(gene_info_dnabd)){
    row = gene_info_dnabd[item,]
    server = "http://rest.ensembl.org"
    ext = "/sequence/region/arabidopsis_thaliana/"
  
    r = GET(paste(server,ext,row$seq_region_name,":",row$start,"..",row$end,sep=""),accept("application/JSON"), config = httr_config)
    response=fromJSON(toJSON(content(r)), flatten = TRUE)
  
    seq = response$seq
    
    if (nchar(seq)>=3){
    syn_length = get_number_of_synonymous_sites(as.character(seq))
    df = data.frame(geneID = as.character(row$geneID), id = as.character(row$id), poly_syn_length_dnabd = syn_length, 
                  poly_ungapped_length_dnabd = row$end - row$start)
    poly_length_dnabd = rbind(df, poly_length_dnabd)
    }
  }
  geneID_list = data.frame(geneID = unique(poly_length_dnabd$geneID), id = unique(poly_length_dnabd$id))

  poly_length_dnabd = poly_length_dnabd[-1]

  poly_length_dnabd = aggregate(.~id, poly_length_dnabd, FUN = sum)
  poly_length_dnabd$poly_nonsyn_length_dnabd = poly_length_dnabd$poly_ungapped_length_dnabd - poly_length_dnabd$poly_syn_length_dnabd

  poly_length_dnabd = merge(geneID_list, poly_length_dnabd, by="id")

  #print(poly_length_dnabd)
  #colnames(poly_length_dnabd) = c("id", "geneID", "poly_syn_length_dnabd", "poly_ungapped_length_dnabd", "poly_nonsyn_length_dnabd")


  #Second for nondnabd
  colnames(gene_info_nondnabd)[2] = "id"

  poly_length_nondnabd=data.frame()

  for (item in 1:nrow(gene_info_nondnabd)){
    row = gene_info_nondnabd[item,]
    server = "http://rest.ensembl.org"
    ext = "/sequence/region/arabidopsis_thaliana/"
  
    r = GET(paste(server,ext,row$seq_region_name,":",row$start,"..",row$end,sep=""),accept("application/JSON"), config = httr_config)
    response=fromJSON(toJSON(content(r)), flatten = TRUE)
  
    seq = response$seq
    
    if (nchar(seq)>=3){
  
      syn_length = get_number_of_synonymous_sites(as.character(seq))
  
      df = data.frame(geneID = as.character(row$geneID), id = as.character(row$id), poly_syn_length_nondnabd = syn_length, 
                  poly_ungapped_length_nondnabd = row$end - row$start)
  
      poly_length_nondnabd = rbind(df, poly_length_nondnabd)
    }
  }
  geneID_list = data.frame(geneID = unique(poly_length_nondnabd$geneID), id = unique(poly_length_nondnabd$id))

  poly_length_nondnabd = poly_length_nondnabd[-1]

  poly_length_nondnabd = aggregate(.~id, poly_length_nondnabd, FUN = sum)
  poly_length_nondnabd$poly_nonsyn_length_nondnabd = poly_length_nondnabd$poly_ungapped_length_nondnabd - poly_length_nondnabd$poly_syn_length_nondnabd

  poly_length_nondnabd = merge(geneID_list, poly_length_nondnabd, by="id")
  #print(poly_length_nondnabd)
  #colnames(poly_ungapped_length_nondnabd) = c("id", "geneID", "poly_syn_length_nondnabd", "poly_ungapped_length_nondnabd", "poly_nonsyn_length_nondnabd")

  #merging lengths
  poly_length = merge(poly_length_dnabd, poly_length_nondnabd, by="geneID")

  #print(poly_length)
  #remvoing the "id" cols here since they are doubled
  poly_length = poly_length[-6]
  colnames(poly_length)[2] = "id"

  #merging count and lengths
  poly_gene = merge(poly_syn_nonsyn_count, poly_length, by.x="id",by.y="id")

  #print(poly_gene)
  #calculating the polymorphism statistics now for four categories: dnabd/ nondnabd and syn/nonsyn
  poly_dnabd_syn_info = poly_syn_nonsyn_info_subset[poly_syn_nonsyn_info_subset$DNABD == "YES" & poly_syn_nonsyn_info_subset$most_severe_consequence=="syn",]
  rownames(poly_dnabd_syn_info) = NULL
  colnames(poly_dnabd_syn_info)[7] = "type"
  poly_dnabd_syn_stats = ldply(lapply(poly_gene$id, function(x) get_polymorphism_stats_per_gene2_dnabd(geneID = x, allele_frequencies = poly_dnabd_syn_info, 
                                                                                                     min_freq=min_freq,variant_type="syn", location="dnabd", poly_gene)), rbind)


  poly_dnabd_nonsyn_info = poly_syn_nonsyn_info_subset[poly_syn_nonsyn_info_subset$DNABD == "YES" & poly_syn_nonsyn_info_subset$most_severe_consequence=="nonsyn",]
  rownames(poly_dnabd_nonsyn_info) = NULL
  colnames(poly_dnabd_nonsyn_info)[7] = "type"
  poly_dnabd_nonsyn_stats = ldply(lapply(poly_gene$id, function(x) get_polymorphism_stats_per_gene2_dnabd(geneID = x, allele_frequencies = poly_dnabd_nonsyn_info, 
                                                                                                     min_freq=min_freq,variant_type="nonsyn", location="dnabd",poly_gene)), rbind)

  poly_nondnabd_syn_info = poly_syn_nonsyn_info_subset[poly_syn_nonsyn_info_subset$DNABD == "NO" & poly_syn_nonsyn_info_subset$most_severe_consequence=="syn",]
  rownames(poly_nondnabd_syn_info) = NULL
  colnames(poly_nondnabd_syn_info)[7] = "type"
  poly_nondnabd_syn_stats = ldply(lapply(poly_gene$id, function(x) get_polymorphism_stats_per_gene2_dnabd(geneID = x, allele_frequencies = poly_nondnabd_syn_info, 
                                                                                                     min_freq=min_freq,variant_type="syn", location="nondnabd", poly_gene)), rbind)

  poly_nondnabd_nonsyn_info = poly_syn_nonsyn_info_subset[poly_syn_nonsyn_info_subset$DNABD == "NO" & poly_syn_nonsyn_info_subset$most_severe_consequence=="nonsyn",]
  rownames(poly_nondnabd_nonsyn_info) = NULL
  colnames(poly_nondnabd_nonsyn_info)[7] = "type"
  poly_nondnabd_nonsyn_stats = ldply(lapply(poly_gene$id, function(x) get_polymorphism_stats_per_gene2_dnabd(geneID = x, allele_frequencies = poly_nondnabd_nonsyn_info, 
                                                                                                     min_freq=min_freq,variant_type="nonsyn", location="nondnabd",poly_gene)), rbind)

  poly_stats_dnabd = merge(poly_dnabd_syn_stats, poly_dnabd_nonsyn_stats, by="id")
  poly_stats_nondnabd = merge(poly_nondnabd_syn_stats, poly_nondnabd_nonsyn_stats, by="id")

  poly_stats = merge(poly_stats_dnabd, poly_stats_nondnabd, by="id")

  poly_gene = merge(poly_gene, poly_stats, by="id")

  return(poly_gene)

}