Alag

Following code is an extension of the mk_dfe project by Stefan, here I will try to do the following on Transcription Factor geneIDs

- Given the geneID, extract the gene specific information (cds co-ordinates, divergent and polymorphic sites and splitting them into syn and nonsyn mutations)

- Retrieve the DNA binding domain information from Uniprot for the given geneID

- Split the gene specific information on the basis of the binding domain positions : binding domain and everything that is not a part of the binding domain (dnabd vs non-dnabd)

- Highlight the proportion of syn and nonsyn mutations within these Domains

- Process the output and prepare files for polyDFE analysis
